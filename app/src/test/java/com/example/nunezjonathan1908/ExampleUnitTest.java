package com.example.nunezjonathan1908;

import com.example.nunezjonathan1908.utils.EventsUtil;
import com.example.nunezjonathan1908.utils.TripUtils;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
//    @Test
//    public void testTripCreation() {
//        String city = "Paris";
//        String stateProvince = "Ile-de-France";
//        String country = "France";
//
//        String location = city + ", " + stateProvince + ", " + country;
//        String locationNoCity = stateProvince + ", " + country;
//        String locationNoStateProvince = city + ", " + country;
//        String locationNoCountry = city + ", " + stateProvince;
//
//        Date currentDate = new Date();
//
//        Trip trip = TripUtils.createTrip(city, stateProvince, country, currentDate);
//        assertEquals(trip.getLocation(), location);
//        assertEquals(trip.getDate(), currentDate);
//
//        trip = TripUtils.createTrip(city, stateProvince, country, null);
//        assertNull(trip.getDate());
//
//        trip = TripUtils.createTrip("", stateProvince, country, currentDate);
//        assertEquals(trip.getLocation(), locationNoCity);
//
//        trip = TripUtils.createTrip(city, "", country, currentDate);
//        assertEquals(trip.getLocation(), locationNoStateProvince);
//
//        trip = TripUtils.createTrip(city, stateProvince, "", currentDate);
//        assertEquals(trip.getLocation(), locationNoCountry);
//    }

//    @Test
//    public void testEventCreation() {
//        String address = "1234 Avenue, City, State Zip";
//        Date date = new Date();
//        Event event = EventsUtil.createTrip(address, date);
//
//        assertEquals(event.getAddress(), address);
//        assertEquals(event.getDate(), date);
//        assertNull(event.getEndDate());
//    }
}