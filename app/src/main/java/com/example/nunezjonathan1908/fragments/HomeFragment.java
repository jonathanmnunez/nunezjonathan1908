package com.example.nunezjonathan1908.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.nunezjonathan1908.R;

import java.util.ArrayList;

//TODO implement custom view for CurrentTrip details
public class HomeFragment extends Fragment {

    public interface HomeFragmentListener {
        void viewCurrentTripNS();
        void addEvent();
        void openSettings();
        void openMap();
        void syncCalEvents();
    }

    private HomeFragmentListener mListener;

    private final View.OnClickListener currentTripClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.viewCurrentTripNS();
        }
    };

    private final View.OnClickListener fabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (currentTripExists()) {
                mListener.addEvent();
            } else {
                mListener.viewCurrentTripNS();
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof HomeFragmentListener) {
            mListener = (HomeFragmentListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_home, menu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        if (view != null) {
            if (currentTripExists()) {
                ((TextView) view.findViewById(R.id.create_new_trip_textView)).setText("View Current Trip");
                mListener.syncCalEvents();
            }
            view.findViewById(R.id.currentTrip_imageButton).setOnClickListener(currentTripClickListener);
            view.findViewById(R.id.fab_add).setOnClickListener(fabClickListener);
            ListView listView = view.findViewById(R.id.listView_archivedTrips);

            ArrayList<String> testArchivedTrips = new ArrayList<>();
            if (getContext() != null) {
                ArrayAdapter<String> testAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, testArchivedTrips);
                listView.setEmptyView(view.findViewById(R.id.empty_archived_trips));
                listView.setAdapter(testAdapter);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_item_open_map) {
            mListener.openMap();
        } else if (item.getItemId() == R.id.menu_item_settings) {
            mListener.openSettings();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean currentTripExists() {
        if (getContext() != null) {
            SharedPreferences sharedPrefs = getContext().getSharedPreferences("currentTrip", Context.MODE_PRIVATE);
            long currentTripId = sharedPrefs.getLong("currentTripId", -1);
            return currentTripId != -1;
        }

        return false;
    }
}
