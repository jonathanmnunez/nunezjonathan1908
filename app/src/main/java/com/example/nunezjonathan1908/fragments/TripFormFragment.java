package com.example.nunezjonathan1908.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.nunezjonathan1908.R;
import com.example.nunezjonathan1908.utils.TripUtils;

import java.util.Date;

public class TripFormFragment extends Fragment {

//    public interface TripFormFragmentListener {
//        void viewCurrentTripWithInputs()
//    }

    private EditText cityInput;
    private EditText stateProvinceInput;
    private EditText countryInput;
    private CalendarView dateInput;

    private View.OnClickListener skipButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private View.OnClickListener nextButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (inputsExists()) {
                TripUtils.createAndSaveCurrentTrip(getContext());
            } else {
                Toast.makeText(getContext(), R.string.toast_no_inputs, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trip_form, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        if (activity != null) {
            activity.setTitle("Trip Form");
        }

        View view = getView();
        if (view != null) {
            cityInput = view.findViewById(R.id.input_city_editText);
            stateProvinceInput = view.findViewById(R.id.input_stateProvince_editText);
            countryInput = view.findViewById(R.id.input_country_editText);
            dateInput = view.findViewById(R.id.input_date_calendarView);

            view.findViewById(R.id.skip_button).setOnClickListener(skipButtonClickListener);
            view.findViewById(R.id.next_button).setOnClickListener(nextButtonClickListener);
        }
    }

    private boolean inputsExists() {
        return !cityInput.getText().toString().isEmpty() || !stateProvinceInput.getText().toString().isEmpty()
                || !countryInput.getText().toString().isEmpty();
    }
}
