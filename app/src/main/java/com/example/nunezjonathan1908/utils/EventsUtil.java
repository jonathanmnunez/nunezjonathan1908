package com.example.nunezjonathan1908.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.example.nunezjonathan1908.Event;
import com.example.nunezjonathan1908.EventListener;

public class EventsUtil {

    private static final String DEBUG_TAG = "EventCreation";

    public static boolean notChecklistEvent(Context _context, Event event) {
        if (event.getEventId() == Event.NO_ID) {
            showNoEditChecklistItemDialog(_context);
        } else {
            return true;
        }

        return false;
    }

    public static Event createEventFromInputs(String title, String address, String date, String time) {
        Event event = new Event();

        //event.generateEventId();
        if (!title.isEmpty()) {
            event.setTitle(title);
        }
        if (!address.isEmpty()) {
            event.setAddress(address);
        }
        if (!date.isEmpty()) {
            event.setDateWithString(date);
        }
        if (!time.isEmpty()) {
            event.setTime(time);
        }

        return event;
    }

    public static void saveEventToItinerary(Context _context, EventListener _listener, Event event) {
        Log.i(DEBUG_TAG, "Event to save: EventId = " + event.getEventId());
        if (ItineraryUtils.saveEventToItinerary(_context, event)) {
            Toast.makeText(_context, "Saved", Toast.LENGTH_SHORT).show();
            if (_listener != null) {
                _listener.eventDidSave();
            }
        }
    }

    public static void editEventToItinerary(Context _context, EventListener _listener, Event eventToEdit) {
        if (ItineraryUtils.editEventFromItinerary(_context, eventToEdit)) {
            _listener.eventDidSave();
        }
    }

    public static void editUpdateEventToItinerary(Context _context, EventListener _listener, Event eventToEdit, long newEventId) {
        if (ItineraryUtils.editUpdateEventFromItinerary(_context, eventToEdit, newEventId)) {
            _listener.eventDidSave();
        }
    }

    public static void deleteEventFromItinerary(final Context _context, final EventListener _listener, final Event eventToDelete) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure you want to delete this Event?");
        builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ItineraryUtils.removeEventFromItinerary(_context, eventToDelete)) {
                    Toast.makeText(_context, "Deleted Event", Toast.LENGTH_SHORT).show();
                    _listener.eventDidDelete();
                } else {
                    Log.i("Event", "Did not delete Event");
                }
            }
        });
        builder.setNegativeButton("CANCEL", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showNoEditChecklistItemDialog(Context _context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle("Notice");
        builder.setMessage("This is a Checklist Item.\nIt can only be modified from the Checklist.");
        builder.setPositiveButton("OK", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
