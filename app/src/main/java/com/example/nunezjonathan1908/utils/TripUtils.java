package com.example.nunezjonathan1908.utils;

import android.content.Context;

import com.example.nunezjonathan1908.Trip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

//TODO save to an Archive file
//TODO save to the cloud
public class TripUtils {

    private static final String FILE_NAME = "currentTrip.dat";

    public static boolean saveCurrentTrip(Context _context, Trip currentTrip) {
        try {
            FileOutputStream fos = _context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);

            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(currentTrip);

            oos.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static Trip loadCurrentTrip(Context _context) {
        try {
            FileInputStream fis = _context.openFileInput(FILE_NAME);

            ObjectInputStream ois = new ObjectInputStream(fis);
            Trip currentTrip = (Trip) ois.readObject();

            ois.close();
            return currentTrip;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Trip createAndSaveCurrentTrip(Context _context) {
        Trip newTrip = new Trip();
        if (saveCurrentTrip(_context, newTrip)) {
            return newTrip;
        }

        return null;
    }

    public static void deleteCurrentTrip(Context _context) {
        File dir = _context.getFilesDir();
        File file = new File(dir, FILE_NAME);
        file.delete();
    }

}
