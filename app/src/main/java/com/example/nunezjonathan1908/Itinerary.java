package com.example.nunezjonathan1908;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Itinerary implements Serializable {

    private static final long serialVersionUID = 1046097385007942206L;

    private ArrayList<Event> events;

    public Itinerary() {
        events = new ArrayList<>();
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void addEvent(Event event) {
        events.add(event);
    }

    public void removeEvent(Event eventToRemove) {
        for (Iterator<Event> iterator = events.iterator(); iterator.hasNext();) {
            Event event = iterator.next();
            if (event.getEventId() == (eventToRemove.getEventId())) {
                iterator.remove();
            }
        }
    }

    public void editEvent(Event eventToEdit) {
        removeEvent(eventToEdit);
        addEvent(eventToEdit);
    }

    public void editEventWithNewEventId(Event eventToEdit, long newEventId) {
        removeEvent(eventToEdit);
        eventToEdit.setEventId(newEventId);
        addEvent(eventToEdit);

    }
}
