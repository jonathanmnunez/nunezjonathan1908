package com.example.nunezjonathan1908;

import com.example.nunezjonathan1908.checklist_items.Budget;
import com.example.nunezjonathan1908.checklist_items.Destination;
import com.example.nunezjonathan1908.checklist_items.Stay;
import com.example.nunezjonathan1908.checklist_items.TimeFrame;
import com.example.nunezjonathan1908.checklist_items.Transportation;

import java.io.Serializable;
import java.util.ArrayList;

public class Checklist implements Serializable {

    private static final long serialVersionUID = 1473725634831834584L;

    public static final ArrayList<String> checklistCategories = new ArrayList<String>() {{
        add(Destination.DESTINATION); add(TimeFrame.TIMEFRAME);
        add(Stay.STAY); add(Transportation.TRANSPORTATION);
        add(Budget.BUDGET);
    }};

    private Destination destination;
    private TimeFrame timeFrame;
    private Stay stay;
    private Transportation transportation;
    private Budget budget;

    public Checklist() {
        destination = new Destination();
        timeFrame = new TimeFrame();
        stay = new Stay();
        transportation = new Transportation();
        budget = new Budget();
    }

    public Destination getDestination() {
        return destination;
    }

    public TimeFrame getTimeFrame() {
        return timeFrame;
    }

    public Stay getStay() {
        return stay;
    }

    public Transportation getTransportation() {
        return transportation;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public void setTimeFrame(TimeFrame timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setStay(Stay stay) {
        this.stay = stay;
    }

    public void setTransportation(Transportation transportation) {
        this.transportation = transportation;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public String getCategoryCompletion(int groupPositon) {
        switch (groupPositon) {
            case 0:
                return destination.getTotalCompleted();
            case 1:
                return timeFrame.getTotalCompleted();
            case 2:
                return stay.getTotalCompleted();
            case 3:
                return transportation.getTotalCompleted();
            case 4:
                return budget.getTotalCompleted();
            default:
                return "0/0";
        }
    }
}
