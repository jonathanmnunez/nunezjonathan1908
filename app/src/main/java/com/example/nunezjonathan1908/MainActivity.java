package com.example.nunezjonathan1908;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.example.nunezjonathan1908.contracts.TripDBContract;
import com.example.nunezjonathan1908.fragments.HomeFragment;
import com.example.nunezjonathan1908.utils.CalendarEventsUtils;
import com.example.nunezjonathan1908.utils.ItineraryUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements HomeFragment.HomeFragmentListener, ChecklistListener {

    private static final int REQUEST_CODE_NEW_TRIP = 0;
    private static final int REQUEST_CODE_SIGNED_OUT = 101;

    private TripsDBManager dbManager;
    private FusedLocationProviderClient fusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbManager = new TripsDBManager(this);

        setTitle("Home");
    }

    @Override
    protected void onResume() {
        super.onResume();

        dbManager.openDB();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_fragment_container, new HomeFragment())
                .commit();
    }

    @Override
    public void viewCurrentTripNS() {
        Intent intent;
        SharedPreferences sharedPrefs = getSharedPreferences("currentTrip", Context.MODE_PRIVATE);
        long currentTripId = sharedPrefs.getLong("currentTripId", -1);
        if (currentTripId != -1) {
            intent = new Intent(this, CurrentTripActivity.class);
            startActivity(intent);
        } else if (createNewTripNS()) {
            intent = new Intent( this, CurrentTripActivity.class);
            startActivity(intent);
        }

        dbManager.closeDB();
    }

    private boolean createNewTripNS() {
        ContentValues values = new ContentValues();
        Calendar today = Calendar.getInstance();
        String format = "MM/dd/yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        values.put(TripDBContract.TripEntry.COLUMN_DATE_CREATED, dateFormat.format(today.getTime()));
        values.put(TripDBContract.TripEntry.COLUMN_TIME_FRAME_CAL_ID, -1);
        values.put(TripDBContract.TripEntry.COLUMN_CHECK_IN_CAL_ID, -1);
        values.put(TripDBContract.TripEntry.COLUMN_CHECK_OUT_CAL_ID, -1);
        values.put(TripDBContract.TripEntry.COLUMN_TRANSPORTATION_CAL_ID, -1);
        long currentTripId = dbManager.insert(values);
        if (currentTripId != -1) {
            SharedPreferences sharedPrefs = getSharedPreferences("currentTrip", Context.MODE_PRIVATE);
            sharedPrefs.edit().putLong("currentTripId", currentTripId).apply();

            return true;
        }

        return false;
    }

    @Override
    public void addEvent() {
        Intent intent = new Intent(this, EventActivity.class);
        intent.setAction(EventActivity.ACTION_NEW_EVENT);
        startActivity(intent);
    }

    @Override
    public void openSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SIGNED_OUT);
    }

    @Override
    public void openMap() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        SharedPreferences sharedPrefs = getSharedPreferences("maps", Context.MODE_PRIVATE);
        if (sharedPrefs.getBoolean("mapsDialog", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Tip\n\n" +
                    "Here's how to help us add events created in your calendar to our app.\n\n" +
                    "When you are saving an event, add this suffix:\n" +
                    "\t\"- rsfb\"\n" +
                    "to your event title, and we'll handle the rest!");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    initiateLastKnowLocation();
                }
            });
            builder.setNegativeButton("Disable Alert", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences sharedPrefs = getSharedPreferences("maps", Context.MODE_PRIVATE);
                    sharedPrefs.edit().putBoolean("mapsDialog", false).apply();
                    initiateLastKnowLocation();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {
            initiateLastKnowLocation();
        }
    }

    @Override
    public void syncCalEvents() {
        CalendarEventsUtils.syncCalendarEvents(getContentResolver(), this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_NEW_TRIP) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.home_fragment_container, new HomeFragment())
                        .commit();
            } else if (requestCode == REQUEST_CODE_SIGNED_OUT){
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        dbManager.closeDB();
    }

    private void initiateLastKnowLocation() {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                String geoUri = "geo:" + location.getLatitude() + "," + location.getLongitude();
                                Uri mapUri = Uri.parse(geoUri);
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                                mapIntent.setPackage("com.google.android.apps.maps");
                                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(mapIntent);
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTripCalId(int token, long eventId, long rowId) {

    }

    @Override
    public void retrieveCalendarEvents(ArrayList<EventNS> foundEvents) {
        if (foundEvents.size() > 0) {
            SharedPreferences sharedPrefs = getSharedPreferences("currentTrip", Context.MODE_PRIVATE);
            long currentTripId = sharedPrefs.getLong("currentTripId", -1);
            if (currentTripId != -1) {
                EventsDBManager dbManager = new EventsDBManager(this);
                dbManager.openDB();
                ArrayList<EventNS> events = dbManager.queryWithTripId(currentTripId);

                Iterator<EventNS> iterator = foundEvents.iterator();
                while (iterator.hasNext()) {
                    EventNS checkEvent = iterator.next();
                    for (EventNS event :
                            events) {
                        if (checkEvent.getTitle().equals(event.getTitle())) {
                            iterator.remove();
                        }
                    }
                }

                for (EventNS event :
                        foundEvents) {
                    dbManager.insert(ItineraryUtils.createEvent(currentTripId, event.getTitle(), event.getAddress(), event.getDatetime(), event.getNotes()));
                }

                dbManager.closeDB();
            }
        }
    }
}
