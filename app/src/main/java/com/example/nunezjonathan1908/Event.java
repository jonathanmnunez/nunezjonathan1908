package com.example.nunezjonathan1908;

import android.content.ContentValues;
import android.provider.CalendarContract;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

//TODO add CompareTo for sorting by Date
public class Event implements Serializable {

    private static final long serialVersionUID = 3043250475003960206L;
    private static final String DATE_FORMAT = "MM/dd/yy";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

    public static final long NO_ID = 0;

    private long eventId = NO_ID;
    private String title = null;
    private String address = null;
    private Date date = null;
    private String time = null;
    private String notes = null;

    public Event() {
    }

    public void generateEventId() {
        Random random = new Random();
        eventId = (random.nextInt((99999-10000) + 1) + 10000) +
                (random.nextInt((9999-1000) + 1) + 1000);
    }

    public long getEventId() {
        return eventId;
    }

    public String getCleanTitle() {
        if (title != null) {
            if (title.contains("- rsfb")) {
                String[] titleSplit = title.split("-");
                return titleSplit[0];
            } else {
                return title;
            }
        } else {
            return "No Title";
        }
    }

    public String getTitle() {
        if (title != null) {
            return title;
        }
        return "No Title";
    }

    public String getAddress() {
        if (address != null) {
            return address;
        }
        return "No Address";
    }

    public Date getDate() {
        return date;
    }

    public String getStartDateString() {
        if (date != null) {
            return dateFormat.format(date);
        }

        return "No Date Set";
    }

    public String getTime() {
        if (time != null) {
            return time;
        }

        return "No Time Set";
    }

    public String getNotes() {
        return notes;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDateWithString(String startDate) {
        try {
            this.date = dateFormat.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
            this.date = new Date();
        }
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        Calendar calendar = Calendar.getInstance();

        if (date != null && time != null) {
            values.put(CalendarContract.Events.TITLE, getTitle());
            if (address != null) {
                values.put(CalendarContract.Events.EVENT_LOCATION, address);
            }
            Calendar dateToExtract = Calendar.getInstance();
            dateToExtract.setTime(date);
            String timeString = time.replace(" ", ":");
            String[] units = timeString.split(":");
            int hour = Integer.parseInt(units[0]);
            int minutes = Integer.parseInt(units[1]);

            if (units[2].equals("PM")) {
                if (hour > 12) {
                    hour = hour + 12;
                }
            }

            calendar.set(dateToExtract.get(Calendar.YEAR), dateToExtract.get(Calendar.MONTH),
                    dateToExtract.get(Calendar.DAY_OF_MONTH), hour, minutes);
            values.put(CalendarContract.Events.DTSTART, calendar.getTimeInMillis());
            values.put(CalendarContract.Events.DTEND, calendar.getTimeInMillis() + 3600000);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getDisplayName());

            return values;
        }

        return null;
    }
}
