package com.example.nunezjonathan1908;

import com.example.nunezjonathan1908.checklist_items.Transportation;

import java.io.Serializable;
import java.util.Date;

public class Trip implements Serializable {

    private static final long serialVersionUID = 1046807634077842284L;

    private Checklist checklist;
    private Itinerary itinerary;

    public Trip() {
        itinerary = new Itinerary();
        checklist = new Checklist();
    }

    public Trip(Checklist _checkList, Itinerary _itinerary) {
        this.checklist = _checkList;
        this.itinerary = _itinerary;
    }

    public long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public Checklist getChecklist() {
        return checklist;
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }

    public void setChecklist(Checklist checklist) {
        this.checklist = checklist;
    }
}
