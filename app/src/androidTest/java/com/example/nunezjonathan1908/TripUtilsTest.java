package com.example.nunezjonathan1908;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.example.nunezjonathan1908.utils.TripUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(MockitoJUnitRunner.class)
public class TripUtilsTest {

    private static final String FILE_NAME = "test.dat";

    private Context appContext;
    private Context mockContext;
    private Trip trip;

    @Before
    public void setup() throws IOException {
        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        mockContext = Mockito.mock(Context.class);

        Mockito.when(mockContext.openFileOutput(Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(appContext.openFileOutput(FILE_NAME, Context.MODE_PRIVATE));

        Mockito.when(mockContext.openFileInput(Mockito.anyString()))
                .thenReturn(appContext.openFileInput(FILE_NAME));

        trip = new Trip();
    }

    @Test
    public void testSaveCurrentTrip() {
        boolean didSave = TripUtils.saveCurrentTrip(mockContext, trip);
        assertTrue(didSave);
    }

    @Test
    public void testCreateAndSaveCurrentTrip() {
        Trip currentTrip = TripUtils.createAndSaveCurrentTrip(mockContext);
        assertNotNull(currentTrip);
    }

    @Test
    public void testLoadCurrentTrip() {
        if (TripUtils.saveCurrentTrip(mockContext, trip)) {
            Trip loadedTrip = TripUtils.loadCurrentTrip(mockContext);
            assertNotNull(loadedTrip);
            assertEquals(loadedTrip.getSerialVersionUID(), trip.getSerialVersionUID());
        }
    }

    @After
    public void deconstruct() {
        File dir = appContext.getFilesDir();
        File file = new File(dir, FILE_NAME);
        file.delete();
    }
}
