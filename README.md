# Resfeber

Resfeber is a traveling companion app and planner. The app operates with
two main focus, Plan Trip & On Trip. Plan Trip provides a checklist with
the most commonly needed items before any trip. An Itinerary is easily 
accessed and editable. On Trip shows the trip in an organized and easy to
read way.

## Getting Started
### Installation
###### Requirements
* Android Studio (latest version)
* Device
    * Android Virtual Device (AVD)
    * Physical Android device
    
###### Instructions
1. Download the project
2. Open Android Studio
3. Select "Open an Existing Android Studio Project"
4. Find the download in your directory

## Tests
### Things to Try
###### Create a New Trip
1. On launch, select the ImageButton the reads "+Create a New Trip"
    - Optionally, select the Floating Action Button
2. Once the "Checklist | Itinerary" tabbed page is reach, a new trip has
been created
###### Complete a Checklist Task
1. On the "Checklist" tab, select an unexpanded view
2. Once expanded, checkable fields will be shown
3. Enter the necessary details
4. Press the "Return" key on the keyboard
###### Add an Event to the Itinerary
1. On the "Itinerary" tab, click the Floating Action Button
2. The Event Form will be displayed
3. After (at least 1) field has been entered, click the "Save" button
###### Create a Calendar Event from Google Maps and add the "- rsfb" suffix
1. From the Home Screen or Itinerary Tab, click the top right Maps icon
2. Find any event you like
3. Locate the "Add Calendar Event" button
4. In the Event creation form, simply place the suffix "- rsfb" in the 
Event Title

### Known Bugs
* Syncing Data to the Cloud will only send the Current Trip data
* The Trip Focus is not functional at this time
* Events with corresponding Calendar Events, when deleted will not delete 
the Calendar event
* The Current Trip cannot be deleted at this time
 

## Built With
* Android Studio v3.4.2
* macOS Mojave v10.14.6

## Authors
* Jonathan Nunez - Designer & Developer

## License
This project is licensed under the MIT License